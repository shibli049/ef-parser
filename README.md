# Access Log Parser

## Description

This is a java tool build on top of spring-boot. To run this project you need:

1. Java version 8,
2. MySQL,
3. Text editor to change configurations.

## How to run

1. Database table : Database table information can be found at `ef_parser.sql` file.
2. Open `config/application.properties` file. Update below information, according to your system configuration:
    1. `spring.datasource.url` : MySQL connection URL
    2. `spring.datasource.username` : MySQL database username
    3. `spring.datasource.password` : MySQL database password
3. You can run `java -jar parser.jar --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500` or directly `./parser.jar --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500`. Note, `config/` folder must be present on the same directory as `parser.jar` file.

## Parameters

1. startDate : is of format "yyyy-MM-dd.HH:mm:ss".
2. duration : "hourly" or "daily".
3. threshold : integer
4. accesslog : /path/to/access/log/file

`accesslog` parameter is optional, other three parameters are mandatory.

## SQL test

1. Write MySQL query to find IPs that mode more than a certain number of requests for a given time period.

Ans: select ip from access_log where request_date >= '2017-01-01 13:00:00' and request_date < '2017-01-01 14:00:00' group by ip having count(*) > 100;

2. Write MySQL query to find requests made by a given IP.

Ans: select * from access_log where ip='192.168.102.136';

## Deliverables

1. Java program that can be run from command line: parser.zip
2. Source Code for the Java program: ef-parser.zip, can also be cloned from: git@gitlab.com:shibli049/ef-parser.git
3. MySQL schema used for the log data : ef_parser.sql
4. SQL queries for SQL test : ef_parser.sql