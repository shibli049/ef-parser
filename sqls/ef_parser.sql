create database ef_parser;
create user 'ef'@'localhost' identified by 'parserDemo';
grant all on ef_parser.* to 'ef'@'localhost';

CREATE TABLE access_log
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  request_date DATETIME NOT NULL,
  ip VARCHAR(15) NOT NULL,
  request VARCHAR(1024) NOT NULL,
  status INT NOT NULL
);


CREATE TABLE access_log_summary
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ip VARCHAR(15) NOT NULL,
    comment VARCHAR(1024) NOT NULL
);



-- SQL tests: 
select ip from access_log where request_date >= '2017-01-01 15:00:00' and request_date < '2017-01-01 16:00:00' group by ip having count(*) > 200;
select ip from access_log where request_date >= '2017-01-01 13:00:00' and request_date < '2017-01-01 14:00:00' group by ip having count(*) > 100;
select ip from access_log where request_date >= '2017-01-01 00:00:00' and request_date < '2017-01-02 00:00:00' group by ip having count(*) > 500;