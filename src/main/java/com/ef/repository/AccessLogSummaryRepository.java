package com.ef.repository;

import com.ef.entity.AccessLogSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by github.com/shibli049 on 3/21/18.
 */

@Repository
public interface AccessLogSummaryRepository extends JpaRepository<AccessLogSummary, Integer> {

}
