package com.ef.repository;

import com.ef.entity.AccessLog;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by github.com/shibli049 on 3/17/18.
 */
@Repository
public interface AccessLogRepository extends JpaRepository<AccessLog, Integer> {


  @Query("select a.ip from AccessLog a where a.requestDate >= :startDate and a.requestDate < :endDate group by a.ip having count(a) > :threshold")
  List<String> findIpThresholdOverBy(
        @Param("startDate") LocalDateTime startDate,
        @Param("endDate") LocalDateTime endDate,
        @Param("threshold") long threshold);

}
