package com.ef;

import com.ef.converter.AccessLogConverter;
import com.ef.entity.Duration;
import com.ef.service.LogParser;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.ef"})
public class Parser implements ApplicationRunner {


  private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);

  private final LogParser logParser;
  private final AccessLogConverter converter;

  private Set<java.lang.String> options = new TreeSet<java.lang.String>();
  private final java.lang.String argumentDateTimeFormat;


  @Autowired
  public Parser(LogParser logParser, AccessLogConverter converter,
      @Value("${datetime.format}") java.lang.String dtFormat) {
    this.logParser = logParser;
    this.converter = converter;
    this.argumentDateTimeFormat = dtFormat;

    options.addAll(Arrays.asList(
        "startDate",
        "duration",
        "threshold"));


  }


  public static void main(java.lang.String[] args) {
    SpringApplication.run(Parser.class, args);
  }


  @Override
  public void run(ApplicationArguments args) throws Exception {
    LOGGER.info("application started.");

    for (java.lang.String name : args.getOptionNames()) {
      LOGGER.debug("name: " + name + ", and option: " + args.getOptionValues(name));
    }

    boolean allOptionAvailable =
        args.getOptionNames().containsAll(options);

    if (!allOptionAvailable) {
      LOGGER.error("options are mandatory." + options);
      return;
    }

    LocalDateTime startTime = converter
        .getDateTime(this.getSingleOptionValue(args, "startDate"), argumentDateTimeFormat);
    java.lang.String durationString = this.getSingleOptionValue(args, "duration");

    Duration duration = getDuration(durationString);
    if (duration == null) {
      LOGGER.error("invalid duration");
      return;
    }

    long threshold;
    try {
      threshold = Long.parseLong(this.getSingleOptionValue(args, "threshold"));
    } catch (NumberFormatException nfe) {
      LOGGER.error("invalid threshold, requires integer");
      return;
    }

    if (args.containsOption("accesslog")) {
      java.lang.String filePath = this.getSingleOptionValue(args, "accesslog");
      long count = logParser.saveLogsAndGetCount(filePath);
      LOGGER.info("total lines: " + count);
      LOGGER.info("all valid data insertion done.");
    }

    LocalDateTime endTime = startTime.plusSeconds(duration.getSeconds());
    List<String> ipList = logParser.findIpsWithThreshold(threshold, duration, startTime, endTime);

    if (ipList != null && !ipList.isEmpty()) {


      String msg = " made more than "
          + threshold + " requests "
          + " starting from " + converter
          .getStringFromDateTime(startTime, argumentDateTimeFormat) +
          ", duration " + duration.name().toLowerCase();
      LOGGER.info(ipList + msg);

      int thresholdCrossed = logParser.saveLogSummary(ipList, msg);
      LOGGER.info(thresholdCrossed + " IPs crossed threshold.");
    }
    LOGGER.info("application finished.");
  }

  private Duration getDuration(java.lang.String durationString) {

    if (durationString == null || durationString.isEmpty()) {
      return null;
    }

    for (Duration duration : Duration.values()) {
      if (duration.name().equalsIgnoreCase(durationString)) {
        return duration;
      }
    }

    return null;
  }

  private java.lang.String getSingleOptionValue(ApplicationArguments args, java.lang.String optionName) {
    List<java.lang.String> values = args.getOptionValues(optionName);

    if (values == null || values.isEmpty()) {
      return null;
    }

    return values.get(0);
  }


}
