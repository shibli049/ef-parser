package com.ef.config;

import com.ef.converter.AccessLogConverter;
import java.util.HashSet;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by github.com/shibli049 on 3/19/18.
 */
@Configuration
@EnableJpaRepositories("com.ef")
public class ConversionConfig {
  private Set<Converter> getConverters(){
    Set<Converter> converters = new HashSet<>();
    converters.add(new AccessLogConverter());
    return converters;
  }

  @Bean
  public ConversionService conversionService(){
    ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
    bean.setConverters(getConverters());
    bean.afterPropertiesSet();
    return bean.getObject();
  }

}
