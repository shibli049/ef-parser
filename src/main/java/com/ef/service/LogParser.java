package com.ef.service;

import com.ef.entity.Duration;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by github.com/shibli049 on 3/18/18.
 */
public interface LogParser {
  long saveLogsAndGetCount(java.lang.String logUrl) throws IOException;
  List<String> findIpsWithThreshold(long requestThreshold, Duration duration, LocalDateTime
      startTime, LocalDateTime endTime);

  int saveLogSummary(List<String> ips, String msg);
}
