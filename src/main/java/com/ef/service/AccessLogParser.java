package com.ef.service;

import com.ef.entity.AccessLog;
import com.ef.entity.AccessLogSummary;
import com.ef.entity.Duration;
import com.ef.repository.AccessLogRepository;
import com.ef.repository.AccessLogSummaryRepository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

/**
 * Created by github.com/shibli049 on 3/18/18.
 */

@Service
public class AccessLogParser implements LogParser {

  private static final Logger LOGGER = LoggerFactory.getLogger(AccessLogParser.class);


  private final ConversionService conversionService;
  private final AccessLogRepository repository;
  private final AccessLogSummaryRepository summaryRepository;


  @Autowired
  public AccessLogParser(ConversionService conversionService,
      AccessLogRepository repository, AccessLogSummaryRepository summaryRepository){
    this.conversionService = conversionService;
    this.repository = repository;
    this.summaryRepository = summaryRepository;
  }



  @Override
  public long saveLogsAndGetCount(java.lang.String logUrl) throws IOException {
    return Files.lines(Paths.get(logUrl))
        .parallel()
        .filter(line -> (line != null && !line.isEmpty()))
        .map(this::convertToAccessLogAndSave)
        .count();
  }

  private AccessLog convertToAccessLogAndSave(java.lang.String line) {
    AccessLog accessLog = conversionService.convert(line, AccessLog.class);
    if(accessLog != null){
      accessLog = repository.save(accessLog);
    }

    LOGGER.debug("access log saved: " + accessLog);

    return accessLog;
  }

  @Override
  public List<String> findIpsWithThreshold(long requestThreshold, Duration duration, LocalDateTime
      startTime, LocalDateTime endTime){
    return repository.findIpThresholdOverBy(startTime, endTime, requestThreshold);
  }

  @Override
  public int saveLogSummary(List<String> ips, String msg) {
    List<AccessLogSummary> summaries = ips.parallelStream()
        .map(ip -> new AccessLogSummary(ip, msg))
        .filter( accessLogSummary -> accessLogSummary != null)
        .collect(Collectors.toList());
    summaries = summaryRepository.save(summaries);
    return summaries.size();
  }
}
