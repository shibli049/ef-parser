package com.ef.entity;

/**
 * Created by github.com/shibli049 on 3/21/18.
 */
public enum Duration {
  DAILY(24*3600),
  HOURLY(1*3600);


  private final Integer seconds;
  private Duration(Integer durationInSeconds){
    this.seconds = durationInSeconds;
  }

  public Integer getSeconds() {
    return seconds;
  }
}
