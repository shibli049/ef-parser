package com.ef.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by github.com/shibli049 on 3/21/18.
 */
@Entity
@Table(name = "access_log_summary")
public class AccessLogSummary implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Integer id;
  @Basic(optional = false)
  @Column(name = "ip")
  private String ip;
  @Basic(optional = false)
  @Column(name = "comment")
  private String comment;

  public AccessLogSummary() {
  }

  public AccessLogSummary(Integer id) {
    this.id = id;
  }

  public AccessLogSummary(Integer id, String ip, String comment) {
    this.id = id;
    this.ip = ip;
    this.comment = comment;
  }

  public AccessLogSummary(String ip, String comment) {
    this.ip = ip;
    this.comment = comment;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AccessLogSummary)) {
      return false;
    }
    AccessLogSummary other = (AccessLogSummary) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "com.ef.entity.AccessLogSummary[ id=" + id + " ]";
  }

}
