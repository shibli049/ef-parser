package com.ef.converter;

import com.ef.entity.AccessLog;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by github.com/shibli049 on 3/18/18.
 */

@Component
public class AccessLogConverter implements Converter<String, AccessLog>{

  public final String logDateTimeFormat = "yyyy-MM-dd HH:mm:ss.SSS";
  public final String delimiter = "\\|";

  @Override
  public AccessLog convert(String source) {
    String[] delimitedSource = source.split(delimiter);

    if(delimitedSource == null || delimitedSource.length != 5){
      return null;
    }

    LocalDateTime dateTime = getDateTime(delimitedSource[0], this.logDateTimeFormat);
    String ip = delimitedSource[1];
    String request = delimitedSource[2];
    int status = Integer.parseInt(delimitedSource[3]);

    AccessLog accessLog = new AccessLog(dateTime, ip, request, status);
    return accessLog;
  }

  public LocalDateTime getDateTime(String dateTimeSrc, String dateTimeFormat){
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
    LocalDateTime date = LocalDateTime.parse(dateTimeSrc, formatter);
    return date;
  }

  public String getStringFromDateTime(LocalDateTime dateTimeSrc, String dateTimeFormat){
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
    return dateTimeSrc.format(formatter);
  }
}
